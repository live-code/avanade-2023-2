import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

/**
 * Questa è una card che fa.....
 */
@Component({
  selector: 'app-card',
  standalone: true,
  imports: [CommonModule, FormsModule, RouterModule],
  template: `
    <p>
      card works!
    </p>
  `,
  styles: [
  ]
})
export class CardComponent {
  /**
   * Il titolo della card
   */
  @Input() title: string = ''
}
