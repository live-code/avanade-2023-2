import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-tabbar',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      tabbar works!
    </p>
  `,
  styles: [
  ]
})
export class TabbarComponent {

}
