import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ButtonModule } from './components/button.module';
import { CardComponent } from './components/card.component';
import { TabbarComponent } from './components/tabbar.component';

@NgModule({
  imports: [
    CardComponent,    // standalone
    ButtonModule,
    CommonModule,
    TabbarComponent,  // standalone
  ],
  exports: [
    CardComponent,
    ButtonModule,
    TabbarComponent,
  ]
})
export class SharedModule { }
