import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CardComponent } from '../../shared/components/card.component';
import { ModalAddComponent } from './modals/modal-add.component';
import { ModalDeleteComponent } from './modals/modal-delete.component';

@Component({
  selector: 'app-uikit',
  template: `
    <p>
      uikit works!
    </p>
    <button mat-icon-button (click)="addUser()">
      <mat-icon fontIcon="add"></mat-icon>
    </button>

    <mat-list role="list"  cdkDropList class="example-list"  (cdkDropListDropped)="drop($event)">
      
      <div  *ngFor="let user of users" cdkDrag class="example-box"  >
        <div class="example-custom-placeholder" *cdkDragPlaceholder></div>

        <mat-list-item role="listitem">
          <div class="flex justify-between items-center">
            {{user.name}}

            <button mat-icon-button (click)="deleteUser(user)">
              <mat-icon fontIcon="delete"></mat-icon>
            </button>
          </div>

        </mat-list-item>
        <mat-divider></mat-divider>
      </div>
      
    </mat-list>
    
    

  `,
  styles: [`
    .example-list {
      width: 500px;
      max-width: 100%;
      border: solid 1px #ccc;
      min-height: 60px;
      display: block;
      background: white;
      border-radius: 4px;
      overflow: hidden;
    }

    .example-box {
      padding: 20px 10px;
      border-bottom: solid 1px #ccc;
      color: rgba(0, 0, 0, 0.87);
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: space-between;
      box-sizing: border-box;
      cursor: move;
      background: white;
      font-size: 14px;
    }

    .cdk-drag-preview {
      box-sizing: border-box;
      border-radius: 4px;
      box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),
      0 8px 10px 1px rgba(0, 0, 0, 0.14),
      0 3px 14px 2px rgba(0, 0, 0, 0.12);
    }

    .cdk-drag-animating {
      transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);
    }

    .example-box:last-child {
      border: none;
    }

    .example-list.cdk-drop-list-dragging .example-box:not(.cdk-drag-placeholder) {
      transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);
    }

    .example-custom-placeholder {
      background: #ccc;
      border: solid 7px red;
      min-height: 60px;
      transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);
    }

  `]
})
export class UikitComponent {
  users: any[] = [];

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
    http.get<any[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => {
        this.users = res;
      })
  }

  save(formData: any) {
    const params = {
      ...formData,
      ids: this.users.map(u => u.id)
    }
    console.log(params)
  }


  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.users, event.previousIndex, event.currentIndex);
  }

  deleteUser(user: any) {
    const dialog = this.dialog.open(ModalDeleteComponent, {
      data: user
    })

    dialog.afterClosed()
      .subscribe(res => {
        if (res) {
          this.http.delete(`https://jsonplaceholder.typicode.com/users/${user.id}`)
            .subscribe(res => {
              this.users = this.users.filter(u => u.id !== user.id);
              this.snackBar.open('ok! deleted', 'DONE', { duration: 1000});
            })
        }
      })
  }

  addUser() {
    const dialog = this.dialog.open(ModalAddComponent, {})

    dialog.afterClosed()
      .subscribe(res => {
        if (!res) return;

        this.http.post(`https://jsonplaceholder.typicode.com/users/`, { name: res})
          .subscribe(newUser => {
            this.users = [...this.users, newUser]
            this.snackBar.open('ok success!', 'DONE', { duration: 1000});
          })
      })
  }
}
