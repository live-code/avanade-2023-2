import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatTabsModule } from '@angular/material/tabs';

import { UikitRoutingModule } from './uikit-routing.module';
import { UikitComponent } from './uikit.component';
import { ModalDeleteComponent } from './modals/modal-delete.component';
import { ModalAddComponent } from './modals/modal-add.component';


@NgModule({
  declarations: [
    UikitComponent,
    ModalDeleteComponent,
    ModalAddComponent
  ],
  imports: [
    MatIconModule,
    DragDropModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatTabsModule,
    MatDividerModule,
    MatListModule,
    FormsModule,
    CommonModule,
    UikitRoutingModule
  ]
})
export class UikitModule { }
