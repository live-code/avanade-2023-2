import { Component } from '@angular/core';

@Component({
  selector: 'app-modal-add',
  template: `
    <h1 mat-dialog-title>Add user </h1>
    <div mat-dialog-content>
      <input type="text" [(ngModel)]="name" class="border-2">
    </div>
    <div mat-dialog-actions>
      <button mat-button [mat-dialog-close]="null">No Thanks</button>
      <button mat-button [mat-dialog-close]="name" cdkFocusInitial>Ok</button>
    </div>
  `,
  styles: [
  ]
})
export class ModalAddComponent {
  name = '';
}
