import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-modal-delete',
  template: `
    <h1 mat-dialog-title>Are you sure to remove {{data.name}}??? </h1>
    <div mat-dialog-content>
      <p>What's your favorite animal?</p>
      
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, ad, alias architecto consequuntur dicta enim eum explicabo, id nam nemo officiis placeat repellat rerum saepe sunt temporibus totam velit voluptatem?
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, ad, alias architecto consequuntur dicta enim eum explicabo, id nam nemo officiis placeat repellat rerum saepe sunt temporibus totam velit voluptatem?
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, ad, alias architecto consequuntur dicta enim eum explicabo, id nam nemo officiis placeat repellat rerum saepe sunt temporibus totam velit voluptatem?
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, ad, alias architecto consequuntur dicta enim eum explicabo, id nam nemo officiis placeat repellat rerum saepe sunt temporibus totam velit voluptatem?
      
    </div>
    <div mat-dialog-actions>
      <button mat-button (click)="onNoClick()">No Thanks</button>
      <button mat-button [mat-dialog-close]="true" cdkFocusInitial>Ok</button>
    </div>
  `,
  styles: [
  ]
})
export class ModalDeleteComponent {
  constructor(
    public dialogRef: MatDialogRef<ModalDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    console.log(data)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
