import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CardComponent } from '../../shared/components/card.component';
import { TabbarComponent } from '../../shared/components/tabbar.component';
import { SharedModule } from '../../shared/shared.module';
import { HomeListComponent } from './components/home-list.component';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    FormsModule,
    RouterModule,
    SharedModule,
    HomeListComponent
    /*CardComponent,
    TabbarComponent*/
  ],
  template: `

    <app-card></app-card>
    <app-tabbar></app-tabbar>
    <app-home-list></app-home-list>
    <p>
      home works!
    </p>
    
    <div>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, natus neque optio perspiciatis quibusdam rem repellendus voluptatum? Consequatur cupiditate, eveniet facilis, fugiat illo molestiae odit optio, perspiciatis quisquam soluta vero?
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, natus neque optio perspiciatis quibusdam rem repellendus voluptatum? Consequatur cupiditate, eveniet facilis, fugiat illo molestiae odit optio, perspiciatis quisquam soluta vero?
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, natus neque optio perspiciatis quibusdam rem repellendus voluptatum? Consequatur cupiditate, eveniet facilis, fugiat illo molestiae odit optio, perspiciatis quisquam soluta vero?
    </div>
  `,
  styles: [
  ]
})
export class HomeComponent {
  label = 'ciao'
}


