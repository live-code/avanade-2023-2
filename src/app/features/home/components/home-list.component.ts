import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-home-list',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      home-list works!
    </p>
  `,
  styles: [
  ]
})
export class HomeListComponent {

}
