import { HttpClient, HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, interval, Observable } from 'rxjs';
import { AuthService } from '../../../core/auth/auth.service';

@Component({
  selector: 'app-signin',
  template: `


    <div class="flex justify-center">
      
      <form 
        #f="ngForm" (submit)="doLogin(f.value)"
        class="flex flex-col gap-4 "
      >

        <div *ngIf="authSrv.pending">loading...</div>
        <div
          *ngIf="authSrv.error"
          class="bg-red-500 p-3 rounded-xl text-white"
        >
          ahia! errore login
        </div>
        
        <h1>SignIn</h1>
        
        <div>
          <div>Username:</div>
          <input
            #usernameInput="ngModel"
            [ngClass]="{error: usernameInput.invalid && f.dirty}"
            required 
            type="text" placeholder="user" ngModel name="username">
        </div>
        
        <div>
          <div>Password:</div>
          <input
            #passwordInput="ngModel"
            [ngClass]="{error: passwordInput.invalid && f.dirty}"
            required type="text" placeholder="pass" ngModel name="password">
        </div>
        
        <button type="submit" class="btn">LOGIN</button>
      </form>
    </div>
    
  `,
  styles: [
  ]
})
export class SigninComponent {
  constructor(

    public authSrv: AuthService,
    private router: Router
  ) {

 /*   this.authSrv.auth$
      .subscribe(val => {
        if (val) {
          this.router.navigateByUrl('home')
        }
      })*/
  }

  async doLogin(formData: { username: string, password: string}) {
    try {
      await this.authSrv.login(
        formData.username,
        formData.password,
      )

     this.router.navigateByUrl('home')
    } catch (e) {
      window.alert(e)
    }
  }

  doLogin2(formData: { username: string, password: string}) {
    this.authSrv.login(
      formData.username,
      formData.password,
    )
      .then((auth) => {
        this.router.navigateByUrl('shop')
      })
      .catch((msg) => {
        window.alert(msg)
      })

    // ....
  }
}
