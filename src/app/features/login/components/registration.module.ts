import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RegistrationComponent } from './registration.component';

@NgModule({
  declarations: [
    RegistrationComponent,

  ],
  imports: [
    RouterModule.forChild([
      { path: '', component: RegistrationComponent }
    ])
  ]
})
export class RegistrationModule {}
