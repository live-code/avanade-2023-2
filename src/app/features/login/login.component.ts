import { Component } from '@angular/core';

@Component({
  selector: 'app-login',
  template: `
    
    <router-outlet></router-outlet>
    
    <hr class="my-3">
    
    <div class="mt-5 flex gap-2 justify-center">
      <button class="btn" routerLink="/login/signin" routerLinkActive="active-btn">SignIn</button>
      <button class="btn" routerLink="registration" routerLinkActive="active-btn">Registration</button>
      <button class="btn" routerLink="lostpass" routerLinkActive="active-btn">LostPass</button>
    </div>
  `,
})
export class LoginComponent {
}
