import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { LoginComponent } from './login.component';
import { RegistrationComponent } from './components/registration.component';
import { LostpassComponent } from './components/lostpass.component';
import { SigninComponent } from './components/signin.component';


@NgModule({
  declarations: [
    LoginComponent,
    LostpassComponent,
    SigninComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    [RouterModule.forChild([

      {
        path: '',
        component: LoginComponent,
        children: [
          { path: 'signin', component: SigninComponent },
          //{ path: 'registration', component: RegistrationComponent },
          { path: 'registration', loadChildren: () => import('./components/registration.module').then(m => m.RegistrationModule)},
          { path: 'lostpass', component: LostpassComponent },
          { path: '', redirectTo: 'signin', pathMatch: 'full' }
        ]
      },
    ])]
  ]
})
export class LoginModule { }
