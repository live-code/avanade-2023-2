import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { catchError, delay, fromEvent, interval, map, of, tap } from 'rxjs';
import { AuthService } from '../../core/auth/auth.service';

@Component({
  selector: 'app-shop',
  template: `
    <p>
      shop works!
    </p>
    
    <li *ngFor="let p of products">{{p.name}}</li>
  `,
  styles: [
  ]
})
export class ShopComponent {
  products: any[] =[]
  constructor(private http: HttpClient, private authService: AuthService) {

/*
    of(1, 2, 3)
      .subscribe(e => console.log(e))*/



    http.get<any[]>('http://localhost:3000/products')
      .subscribe({
        next: (res) => {
          // console.log('SUCCESS', res)
          this.products = res;
        },
        error: err => {
          // console.log('ERRORE', err)
        },
      })
  }
}
