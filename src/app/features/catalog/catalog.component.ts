import { Component } from '@angular/core';

@Component({
  selector: 'app-catalog',
  template: `
    <p>
      catalog works!
    </p>
    
    <app-button></app-button>
    
    <app-catalog-form></app-catalog-form>
    <app-catalog-list></app-catalog-list>
  `,
})
export class CatalogComponent { }
