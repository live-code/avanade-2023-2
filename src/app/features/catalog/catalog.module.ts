import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { CatalogFormComponent } from './components/catalog-form.component';
import { CatalogListComponent } from './components/catalog-list.component';

@NgModule({
  declarations: [
    CatalogComponent,
    CatalogListComponent,
    CatalogFormComponent,
  ],
  imports: [
    SharedModule,
    CatalogRoutingModule
  ]
})
export class CatalogModule {

}
