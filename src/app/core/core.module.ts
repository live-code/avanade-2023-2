import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CardComponent } from '../shared/components/card.component';
import { NavbarComponent } from './components/navbar.component';
import { IfLoggedDirective } from './auth/if-logged.directive';
import { IfSigninDirective } from './auth/if-signin.directive';

@NgModule({
  declarations: [
    NavbarComponent,
    IfLoggedDirective,
    IfSigninDirective
  ],
  exports: [
    NavbarComponent,
    IfLoggedDirective,
    IfSigninDirective
  ],
  imports: [
    CardComponent,
    CommonModule,
    FormsModule,
    RouterModule
  ]
})
export class CoreModule { }
