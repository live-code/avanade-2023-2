import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-navbar',
  template: `
    <div class="flex flex-row gap-4 m-3">
      <button *appIfSignin class="btn" routerLink="home"  routerLinkActive="!bg-red-500 text-white">Home</button>
      <button class="btn" routerLink="shop" appIfLogged routerLinkActive="!bg-red-500 text-white">Shop</button>
      <button class="btn" routerLink="catalog" appIfLogged routerLinkActive="!bg-red-500 text-white">Catalog</button>
      <button class="btn" routerLink="contacts" routerLinkActive="!bg-red-500 text-white">contacts</button>
      <button class="btn" routerLink="uikit" routerLinkActive="!bg-red-500 text-white">crud dnd angular material</button>
      <button class="btn" *ngIf="!authSrv.isLogged" routerLink="login" routerLinkActive="!bg-red-500 text-white">login</button>
      <button class="btn" 
              (click)="logoutHandler()">logout</button>

      <!--{{authSrv.displayName}}-->
      {{authSrv.displayName$ | async}}
    </div>
    
  `,
  styles: [
  ]
})
export class NavbarComponent {

  constructor(public authSrv: AuthService, private router: Router) {
  }

  logoutHandler() {
    this.authSrv.logout();
    this.router.navigateByUrl('login')
  }
}
