import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { catchError, delay, map, Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authSrv: AuthService,
    private snackbar: MatSnackBar,
    private router: Router
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const isLogged = this.authSrv.isLogged;
    const tk = this.authSrv.token;

    let cloneReq = req;

    if (isLogged && tk && req.url.includes('localhost')) {
       cloneReq = req.clone({
        setHeaders: {
          Authorization: tk
        }
      })
    }

    return next.handle(cloneReq)
      .pipe(
        catchError(err => {
          switch (err.status) {
            default:
            case 401:
            case 404:
              this.snackbar.open(err.status, 'AHIA', { duration: 1000})
              this.router.navigateByUrl('login')
              this.authSrv.logout();
              break;
          }
          return throwError(err)
        })
      )
  }

}
