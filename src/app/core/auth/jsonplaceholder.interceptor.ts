import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay, map, Observable } from 'rxjs';

@Injectable()
export class JsonplaceholderInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let cloneReq = req;
    if (req.url.includes('jsonplaceholder')) {
       cloneReq = req.clone({
        setHeaders: {
          APIKEY: 'abc123'
        }
      })
    }
    return next.handle(cloneReq)
  }

}
