import { Directive, ElementRef, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { distinctUntilChanged, map, mergeMap } from 'rxjs';
import { AuthService } from './auth.service';

/**
 *
 * USAGE
 *
 * <div *appIfSignIn>
 */
@Directive({
  selector: '[appIfSignin]'
})
export class IfSigninDirective {
  constructor(
    private tpl: TemplateRef<any>,
    private el: ElementRef,
    private view: ViewContainerRef,
    private authService: AuthService
  ) {

      authService.isLogged$
        .subscribe(isLogged => {
          console.log(isLogged)
          if (isLogged) {
            view.createEmbeddedView(tpl)
          }
          else {
            view.clear()
          }
        })


  }

}
