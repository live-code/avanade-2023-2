import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, distinctUntilChanged, map, Observable, ReplaySubject } from 'rxjs';
import { Auth, Role } from '../../model/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth: Auth | null = null;
  // o---o---o------->
  auth$ = new BehaviorSubject<Auth | null>(null);
  // auth$ = new ReplaySubject<Auth | null>(3);
  error: boolean = false;
  pending = false;

  constructor(
    private http: HttpClient,
  ) {
    const authFromLocalStorage = localStorage.getItem('auth');
    if (authFromLocalStorage) {
      this.auth = JSON.parse(authFromLocalStorage) as Auth;
      this.auth$.next(JSON.parse(authFromLocalStorage))
    }
  }


  login(username: string, password: string): Promise<Auth> {
    return new Promise((resolve, reject) => {
      this.error = false;
      this.pending = true;

      const params = new HttpParams()
        .set('username', username)
        .set('password', password)

      this.http.get<Auth>('http://localhost:3000/login', { params })
        .subscribe({
          next: (res) => {
            this.auth = res;
            this.auth$.next(res)
            this.pending = false;

            localStorage.setItem('auth', JSON.stringify(res))
            resolve(res)
          },
          error: (err) => {
            this.pending = false;
            this.error = true;
            reject('ahia!!!!')
          }
        })
    })

  }

  logout() {
    this.auth = null;
    this.auth$.next(null);
    localStorage.removeItem('auth')
  }

  get isLogged(): boolean {
    return !!this.auth;
  }

  get isLogged$() {
    return this.auth$
      .pipe(
        map(data => !!data),
        distinctUntilChanged(),
      )
  }


  get role(): Role | null {
    return this.auth?.role ? this.auth.role : null;
  }

  get token(): string | undefined {
    return this.auth?.token
  }

  get token$(): Observable<string | undefined> {
    return this.auth$
      .pipe(
        map(auth => auth?.token)
      )
  }

  get displayName(): string | undefined {
    return this.auth?.displayName
  }

  get displayName$(): Observable<string | undefined> {
    return this.auth$
      .pipe(
        map(auth => auth?.displayName)
      )
  }


}
