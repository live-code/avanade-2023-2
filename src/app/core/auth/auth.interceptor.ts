import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import {
  catchError,
  delay,
  first,
  iif,
  map,
  mergeMap,
  Observable,
  of,
  take,
  tap,
  throwError,
  withLatestFrom
} from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authSrv: AuthService,
    private snackbar: MatSnackBar,
    private router: Router
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return this.authSrv.isLogged$
      .pipe(
        // take(1),
        first(),
        withLatestFrom(this.authSrv.token$),
        mergeMap(([isLogged, tk]) => {
          return iif(
            () => isLogged,
            next.handle(req.clone({ setHeaders: { Authorization: tk! } })),
            next.handle(req)
          )
        }),
        catchError(err => {
          switch (err.status) {
            default:
            case 401:
            case 404:
              this.snackbar.open(err.status, 'AHIA', { duration: 1000})
              this.router.navigateByUrl('login')
              this.authSrv.logout();
              break;
          }
          return throwError(err)
        })
      )

  }

}
