import { HttpClient } from '@angular/common/http';
import { inject, Injectable, Injector } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable, tap } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({providedIn: 'root' })
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate() {
    return this.authService.isLogged$
      .pipe(
        tap(val => {
         if(!val) {
           this.router.navigateByUrl('login')
         }
        })
      );
  }


  // without obs
  canActivateWithoutObs() {
    const isLogged = this.authService.isLogged;
    if (!isLogged) {
      this.router.navigateByUrl('login')
    }
    return isLogged;
  }

}

// =============

export const loggedGuard = () => {
  const router = inject(Router);
  return inject(AuthService).isLogged$
    .pipe(
      tap(val => {
        if (!val) {
          router.navigateByUrl('login')
        }
      })
    )
}







export const loggedGuardWithoutObs = () => {
  const isLogged = inject(AuthService).isLogged;
  if (!isLogged) {
    inject(Router).navigateByUrl('login')
  }
  return isLogged;
}
