import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, loggedGuard } from './core/auth/auth.guard';
import { AuthService } from './core/auth/auth.service';

export const APP_ROUTES: Routes = [
  { path: 'home', loadComponent: () => import('./features/home/home.component').then(c => c.HomeComponent) },
  { path: 'contacts', loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule) },
  { path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule) },
  {
    path: 'login',
    loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule),
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'catalog',
    loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule),
    canActivate: [loggedGuard]
  },
  {
    path: 'shop',
    loadChildren: () => import('./features/shop/shop.module').then(m => m.ShopModule),
    canActivate: [AuthGuard]
  }
]
@NgModule({
  imports: [
    RouterModule.forRoot(APP_ROUTES)
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {}
