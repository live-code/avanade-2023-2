
export type Role = 'admin' | 'guest';

export interface Auth {
  token: string;
  displayName: string;
  role: Role;
}
