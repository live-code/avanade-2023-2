import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AuthInterceptor } from './core/auth/auth.interceptor';
import { JsonplaceholderInterceptor } from './core/auth/jsonplaceholder.interceptor';
import { CoreModule } from './core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CoreModule,
    HttpClientModule,
    BrowserModule,
    MatSnackBarModule,
    AppRoutingModule,
    BrowserAnimationsModule,
  ],
  providers: [
    // AuthService,
    // { provide: AuthService, useClass: AuthService}
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    // { provide: AuthService, useClass: AuthService}
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JsonplaceholderInterceptor,
      multi: true
    },
    // { provide: HTTP_INTERCEPTORS, useClass: AzureInterceptor },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

