'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">avanade-23-2 documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-4c54d0046760bed8e05217d894d66d024a7bff439d55700e8e28e0f8d8e4261a62a0720ff3a36ea7e0e2576552aa3ddd8585cda10cfd6736c2147ace7b39994e"' : 'data-target="#xs-components-links-module-AppModule-4c54d0046760bed8e05217d894d66d024a7bff439d55700e8e28e0f8d8e4261a62a0720ff3a36ea7e0e2576552aa3ddd8585cda10cfd6736c2147ace7b39994e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-4c54d0046760bed8e05217d894d66d024a7bff439d55700e8e28e0f8d8e4261a62a0720ff3a36ea7e0e2576552aa3ddd8585cda10cfd6736c2147ace7b39994e"' :
                                            'id="xs-components-links-module-AppModule-4c54d0046760bed8e05217d894d66d024a7bff439d55700e8e28e0f8d8e4261a62a0720ff3a36ea7e0e2576552aa3ddd8585cda10cfd6736c2147ace7b39994e"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ButtonModule.html" data-type="entity-link" >ButtonModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ButtonModule-3e6ac8165fdf51b260ea3f3074290b4e5fc421bc55011aae2bf24940937c8ee931ffe74f52ec010a644e9e0a37b84a63e77704145aad0d736bab9202517830ec"' : 'data-target="#xs-components-links-module-ButtonModule-3e6ac8165fdf51b260ea3f3074290b4e5fc421bc55011aae2bf24940937c8ee931ffe74f52ec010a644e9e0a37b84a63e77704145aad0d736bab9202517830ec"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ButtonModule-3e6ac8165fdf51b260ea3f3074290b4e5fc421bc55011aae2bf24940937c8ee931ffe74f52ec010a644e9e0a37b84a63e77704145aad0d736bab9202517830ec"' :
                                            'id="xs-components-links-module-ButtonModule-3e6ac8165fdf51b260ea3f3074290b4e5fc421bc55011aae2bf24940937c8ee931ffe74f52ec010a644e9e0a37b84a63e77704145aad0d736bab9202517830ec"' }>
                                            <li class="link">
                                                <a href="components/ButtonComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ButtonComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CatalogModule.html" data-type="entity-link" >CatalogModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CatalogModule-94a8d5b5dc1ff7f97535a9c190a9226779625bf54444fc0a335c0921a3b21d60f428bf9251729de1cc367c68da05d346312217e96da4c08848367b5ce65cb00d"' : 'data-target="#xs-components-links-module-CatalogModule-94a8d5b5dc1ff7f97535a9c190a9226779625bf54444fc0a335c0921a3b21d60f428bf9251729de1cc367c68da05d346312217e96da4c08848367b5ce65cb00d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CatalogModule-94a8d5b5dc1ff7f97535a9c190a9226779625bf54444fc0a335c0921a3b21d60f428bf9251729de1cc367c68da05d346312217e96da4c08848367b5ce65cb00d"' :
                                            'id="xs-components-links-module-CatalogModule-94a8d5b5dc1ff7f97535a9c190a9226779625bf54444fc0a335c0921a3b21d60f428bf9251729de1cc367c68da05d346312217e96da4c08848367b5ce65cb00d"' }>
                                            <li class="link">
                                                <a href="components/CatalogComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CatalogFormComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CatalogListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogListComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CatalogRoutingModule.html" data-type="entity-link" >CatalogRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ContactsModule.html" data-type="entity-link" >ContactsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ContactsModule-7ec8d83cad343a9d5ec2c908a12b169739a028450b745bd5bda9a5ed125f0063a20e590402ced564b7b3be847aae749e977fbab36605226244a6de3271676397"' : 'data-target="#xs-components-links-module-ContactsModule-7ec8d83cad343a9d5ec2c908a12b169739a028450b745bd5bda9a5ed125f0063a20e590402ced564b7b3be847aae749e977fbab36605226244a6de3271676397"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ContactsModule-7ec8d83cad343a9d5ec2c908a12b169739a028450b745bd5bda9a5ed125f0063a20e590402ced564b7b3be847aae749e977fbab36605226244a6de3271676397"' :
                                            'id="xs-components-links-module-ContactsModule-7ec8d83cad343a9d5ec2c908a12b169739a028450b745bd5bda9a5ed125f0063a20e590402ced564b7b3be847aae749e977fbab36605226244a6de3271676397"' }>
                                            <li class="link">
                                                <a href="components/ContactsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ContactsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ContactsRoutingModule.html" data-type="entity-link" >ContactsRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link" >CoreModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CoreModule-90e135d72a0b68857461ce7aa9e58fe2f39e8b8a9adfda852694330827cf880a9bf7c48a5a34262b71c939f444229e66d0166b3c7bc829ead437765519dd55f7"' : 'data-target="#xs-components-links-module-CoreModule-90e135d72a0b68857461ce7aa9e58fe2f39e8b8a9adfda852694330827cf880a9bf7c48a5a34262b71c939f444229e66d0166b3c7bc829ead437765519dd55f7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CoreModule-90e135d72a0b68857461ce7aa9e58fe2f39e8b8a9adfda852694330827cf880a9bf7c48a5a34262b71c939f444229e66d0166b3c7bc829ead437765519dd55f7"' :
                                            'id="xs-components-links-module-CoreModule-90e135d72a0b68857461ce7aa9e58fe2f39e8b8a9adfda852694330827cf880a9bf7c48a5a34262b71c939f444229e66d0166b3c7bc829ead437765519dd55f7"' }>
                                            <li class="link">
                                                <a href="components/CardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NavbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginModule.html" data-type="entity-link" >LoginModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginModule-14c6a103aa9072ffd76ca8c2cbdea79bc84ad93ad0924af9fb64a6ae0a2dd0dc6e874fd05b00acac4bb93efb2045341d85d05995dca7ae04274084c51a9e7a5c"' : 'data-target="#xs-components-links-module-LoginModule-14c6a103aa9072ffd76ca8c2cbdea79bc84ad93ad0924af9fb64a6ae0a2dd0dc6e874fd05b00acac4bb93efb2045341d85d05995dca7ae04274084c51a9e7a5c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginModule-14c6a103aa9072ffd76ca8c2cbdea79bc84ad93ad0924af9fb64a6ae0a2dd0dc6e874fd05b00acac4bb93efb2045341d85d05995dca7ae04274084c51a9e7a5c"' :
                                            'id="xs-components-links-module-LoginModule-14c6a103aa9072ffd76ca8c2cbdea79bc84ad93ad0924af9fb64a6ae0a2dd0dc6e874fd05b00acac4bb93efb2045341d85d05995dca7ae04274084c51a9e7a5c"' }>
                                            <li class="link">
                                                <a href="components/LoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LostpassComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LostpassComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SigninComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SigninComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RegistrationModule.html" data-type="entity-link" >RegistrationModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-RegistrationModule-ca109fed5360bb531287452764f4afca3f41ecb5af47e160b23fded13053b83ab6660a2bbf4d4af3873a479a1a37715d51c2756c519c4dbb710bdcee2fbbf4cb"' : 'data-target="#xs-components-links-module-RegistrationModule-ca109fed5360bb531287452764f4afca3f41ecb5af47e160b23fded13053b83ab6660a2bbf4d4af3873a479a1a37715d51c2756c519c4dbb710bdcee2fbbf4cb"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RegistrationModule-ca109fed5360bb531287452764f4afca3f41ecb5af47e160b23fded13053b83ab6660a2bbf4d4af3873a479a1a37715d51c2756c519c4dbb710bdcee2fbbf4cb"' :
                                            'id="xs-components-links-module-RegistrationModule-ca109fed5360bb531287452764f4afca3f41ecb5af47e160b23fded13053b83ab6660a2bbf4d4af3873a479a1a37715d51c2756c519c4dbb710bdcee2fbbf4cb"' }>
                                            <li class="link">
                                                <a href="components/RegistrationComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RegistrationComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-24313d2b310e210e0c33d420fb3f78621c0b939c3def1a9016848b31207341d1bbe46670b74a15981dbdc637d4993ceb7a746af36ed4f045aff44f54e26d741c"' : 'data-target="#xs-components-links-module-SharedModule-24313d2b310e210e0c33d420fb3f78621c0b939c3def1a9016848b31207341d1bbe46670b74a15981dbdc637d4993ceb7a746af36ed4f045aff44f54e26d741c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-24313d2b310e210e0c33d420fb3f78621c0b939c3def1a9016848b31207341d1bbe46670b74a15981dbdc637d4993ceb7a746af36ed4f045aff44f54e26d741c"' :
                                            'id="xs-components-links-module-SharedModule-24313d2b310e210e0c33d420fb3f78621c0b939c3def1a9016848b31207341d1bbe46670b74a15981dbdc637d4993ceb7a746af36ed4f045aff44f54e26d741c"' }>
                                            <li class="link">
                                                <a href="components/CardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabbarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ShopModule.html" data-type="entity-link" >ShopModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ShopModule-8a49ddadd753d69a8affd4e7fb68e7c224ac2a4f04e889206dd8026ac5627775dca536f4df1f779e2d0adacee8117f1a30f05e70bee38a5ebd7d8b2b69ea2c8c"' : 'data-target="#xs-components-links-module-ShopModule-8a49ddadd753d69a8affd4e7fb68e7c224ac2a4f04e889206dd8026ac5627775dca536f4df1f779e2d0adacee8117f1a30f05e70bee38a5ebd7d8b2b69ea2c8c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ShopModule-8a49ddadd753d69a8affd4e7fb68e7c224ac2a4f04e889206dd8026ac5627775dca536f4df1f779e2d0adacee8117f1a30f05e70bee38a5ebd7d8b2b69ea2c8c"' :
                                            'id="xs-components-links-module-ShopModule-8a49ddadd753d69a8affd4e7fb68e7c224ac2a4f04e889206dd8026ac5627775dca536f4df1f779e2d0adacee8117f1a30f05e70bee38a5ebd7d8b2b69ea2c8c"' }>
                                            <li class="link">
                                                <a href="components/ShopComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ShopComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ShopRoutingModule.html" data-type="entity-link" >ShopRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UikitModule.html" data-type="entity-link" >UikitModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UikitModule-809a61707d94a48c917e47db7524868cd5b57f911eef31147b0812c04b53852f3a840becf63f4ced1b95dce8b1bf982524f50929aa843a380243fdf5132f4477"' : 'data-target="#xs-components-links-module-UikitModule-809a61707d94a48c917e47db7524868cd5b57f911eef31147b0812c04b53852f3a840becf63f4ced1b95dce8b1bf982524f50929aa843a380243fdf5132f4477"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UikitModule-809a61707d94a48c917e47db7524868cd5b57f911eef31147b0812c04b53852f3a840becf63f4ced1b95dce8b1bf982524f50929aa843a380243fdf5132f4477"' :
                                            'id="xs-components-links-module-UikitModule-809a61707d94a48c917e47db7524868cd5b57f911eef31147b0812c04b53852f3a840becf63f4ced1b95dce8b1bf982524f50929aa843a380243fdf5132f4477"' }>
                                            <li class="link">
                                                <a href="components/ModalAddComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ModalAddComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ModalDeleteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ModalDeleteComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UikitComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UikitComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UikitRoutingModule.html" data-type="entity-link" >UikitRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/CardComponent.html" data-type="entity-link" >CardComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/HomeComponent.html" data-type="entity-link" >HomeComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/HomeListComponent.html" data-type="entity-link" >HomeListComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/TabbarComponent.html" data-type="entity-link" >TabbarComponent</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link" >AuthService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/AuthInterceptor.html" data-type="entity-link" >AuthInterceptor</a>
                            </li>
                            <li class="link">
                                <a href="interceptors/JsonplaceholderInterceptor.html" data-type="entity-link" >JsonplaceholderInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link" >AuthGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Auth.html" data-type="entity-link" >Auth</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});